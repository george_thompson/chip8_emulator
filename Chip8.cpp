#include <cstdint>
#include <cstring>
#include <fstream>
#include <stdio.h>
#include <bitset>
#include <iostream>
//#include "OP_codes.h"

using namespace std;
//credit austin morlan for guide
class Chip8
{
  public:
    uint8_t  registers[16]{};
    uint8_t  memory[4096]{};
    uint16_t index{};
    uint16_t pc{};
    uint16_t stack[16]{};
    uint8_t  sp{};
    uint8_t  delaytimer{};
    uint8_t  soundtimer{};
    uint8_t  keypad[16]{};
    uint32_t screen[8 * 4]{};
    uint16_t opcode;

    Chip8();
    void LoadROM(char const* filename);
    void OP_00E0();
    void OP_1nnn();
    void OP_6xkk();
    void OP_7xkk();
    void OP_Annn();
    void OP_dxyn();

};



const unsigned int START_ADDR = 0x200;


// load fontset
const unsigned int FONTSET_SIZE = 80;
uint8_t fontset[FONTSET_SIZE] =
{
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

const unsigned int FONTSET_START_ADDR = 0x50;

// initialize program counter
Chip8::Chip8(){
  pc = START_ADDR;
  for (unsigned int i = 0; i < FONTSET_SIZE; ++i)
  {
    memory[FONTSET_START_ADDR+i] = fontset[i];
  }
}


// Load Rom into Memory
void Chip8::LoadROM(char const* filename)

{
  std::ifstream file(filename, std::ios::binary | std::ios::ate);

  if(file.is_open())
  {
    std::streampos size = file.tellg();
    char* buffer = new char[size];

    file.seekg(0, std::ios::beg);
    file.read(buffer, size);
    file.close();
    for (long i = 0; i < size; ++i){
      memory[START_ADDR + i] = buffer[i];
      //printf(" %0lX: %0X/n", (START_ADDR+i), memory[START_ADDR+i]);
    }

  delete[] buffer;
    //for (long i = 0; i < size; ++i){
      //printf(" %0X\n" , memory[START_ADDR+i]);
    //}
    }
}
//----------
// Opcodes
//----------
void Chip8::OP_00E0()
{
  memset(screen, 0, sizeof(screen));
}

// Jump to nnn
void Chip8::OP_1nnn()
{
  uint16_t addr = opcode & 0x0FFFu;
  pc = addr;
}

// stores the value kk into register x
  //0101 1010 1001 1010
void Chip8::OP_6xkk()
{
  uint16_t value      =  opcode & 0x00FFu;
  uint8_t target_addr = (opcode & 0x0F00u)>>8;
  registers[target_addr] = value;
}

// adds value kk with the val at register x then stores
// result in register x
void Chip8::OP_7xkk()
{
  uint16_t value      =   opcode & 0x00FFu;
  uint8_t  target_addr = (opcode & 0x0F00u)>>8;
  uint16_t reg_add_result = value + registers[target_addr];
  registers[target_addr] = reg_add_result;
}

// Stores nnn in register I
void Chip8::OP_Annn()
{
  uint16_t value = opcode & 0x0FFFu;
  index = value;
}

// display n-byte sprite dxyn
#define SCREEN_HEIGHT = 64
#define SCREEN_WIDTH  = 32
void Chip8::OP_dxyn()
{
  uint8_t sprite_height = opcode & 0x000Fu;
  uint8_t  xcord  = (opcode & 0x0F00u)>>8u;
  uint8_t  ycord  = (opcode & 0x00F0u)>>4u;

  uint16_t mem_location = index;
  uint8_t  screen_x  = registers[xcord] & 63;
  uint8_t  screen_y  = registers[ycord] & 31;
  registers[0xf] = 0;

  for (int i = 0; i < sprite_height; i++){
    uint8_t spr_byte = memory[index];
    for (int j = 0; j < 8;j++){
      uint8_t sprite_pix = (spr_byte>>j) + 0x8u;
      uint32_t screen_pix = screen[j + 8*i];
      screen[j + (8*i)] = sprite_pix^screen_pix;
    }
   }
}




int main(){
  Chip8 chip8; ; 
  chip8.LoadROM("IBMLogo");
  
  while (chip8.pc < 0x220){
    uint16_t nib1  =  chip8.memory[chip8.pc];
    uint16_t nib2  =  chip8.memory[chip8.pc+1];
    uint16_t OP = (nib1<<8) | nib2;
    uint8_t code = (OP & 0x4 )>>8;
    std::bitset<16> x(OP);
    
    printf("%0X ",OP);
    cout << x << "\n";


    if(code == 6){
      printf("instruction!\n");
    }
    chip8.pc += 2;
  }
  
  return 0;
}
